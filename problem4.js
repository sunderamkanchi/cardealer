function problem4(inventory) {
  if (inventory === undefined) {
    return [];
  } else if (inventory.length === 0) {
    return [];
  } else {
    let inventoryYears = [];
    for (let i = 0; i < inventory.length; i++) {
      let file = inventory[i];
      inventoryYears.push(file.car_year);
    }
    return inventoryYears;
  }
}

module.exports = problem4;
