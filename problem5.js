function problem5(years) {
  if (years === undefined) {
    return [];
  } else if (years.length === 0) {
    return [];
  } else {
    let oldCar = [];
    for (let i = 0; i < years.length; i++) {
      if (years[i] < 2000) {
        oldCar.push(years[i]);
      }
    }
    return [oldCar.length];
  }
}
module.exports = problem5;
