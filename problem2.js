function problem2(inventory) {
  if (inventory === undefined) {
    return [];
  } else if (inventory.length === 0) {
    return [];
  } else {
    return inventory[inventory.length - 1];
  }
}

module.exports = problem2;
