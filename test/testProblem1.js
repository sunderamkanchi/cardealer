let inventory = require("../input");
//let inventory = [];
let id = 33;
let problem1 = require("../problem1");

let result = problem1(inventory, id);

if (result.length !== 0) {
  let obj = `Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`;
  let final = { obj };

  console.log(final);
} else {
  console.log(result);
}
