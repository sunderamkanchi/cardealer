let inventory = require("../input");
//let inventory = [];
let problem2 = require("../problem2");

let result = problem2(inventory);

if (result.length !== 0) {
  let obj = `Last car is a ${result.car_make} ${result.car_model}`;
  let final = { obj };

  console.log(final);
} else {
  console.log(result);
}
