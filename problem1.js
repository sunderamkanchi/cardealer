function problem1(inventory, id) {
  if (inventory === undefined && id === undefined) {
    return [];
  } else if (inventory === undefined || id === undefined) {
    return [];
  } else if (inventory.length === 0) {
    return [];
  }

  {
    let fullid = false;
    for (let i = 0; i < inventory.length; i++) {
      let entry = inventory[i];
      if (entry.id === id) {
        fullid = true;
        let result = inventory[i];
        return result;
      }
    }
    if (fullid !== true) {
      return [];
    }
  }
}

module.exports = problem1;
