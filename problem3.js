function problem3(inventory) {
  if (inventory === undefined) {
    return [];
  } else if (inventory.length === 0) {
    return [];
  } else {
    let sorting = [];
    for (let i = 0; i < inventory.length; i++) {
      let array = inventory[i];
      sorting.push(array.car_model);
    }
    return sorting.sort();
  }
}

module.exports = problem3;
